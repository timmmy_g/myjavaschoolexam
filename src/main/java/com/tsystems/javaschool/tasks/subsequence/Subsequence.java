package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x == null || y == null) {
            throw new IllegalArgumentException();
        }

        List list = new ArrayList();

        int newIndexForY = 0;

        for (int i = 0; i < x.size(); i++) {

            for (int k = newIndexForY; k < y.size(); k++) {

                if (x.get(i).equals(y.get(k))) {
                    list.add(x.get(i));
                    newIndexForY = k++;
                    break;
                }
            }
        }

        if (list.size() == x.size()) {
            for (int i = 0; i < x.size(); i++) {
                if (!x.get(i).equals(list.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
