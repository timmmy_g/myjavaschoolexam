package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
		
		// checking the statement for all invalid cases
        if(statement == null || statement.isEmpty() || statement.matches(".*[^0123456789+\\-*/().].*") ||

        statement.matches(".*[+\\-/*.]{2}.*") || statement.matches(".*\\)\\d.*") ||

        statement.matches(".*\\d\\(.*") || statement.matches(".*[+\\-/*.][)].*") ||

        statement.matches(".*[(][+/*.].*") || statement.matches(".*[.][()].*") ||

        statement.matches(".*[()][.].*") || statement.matches("[^(\\d\\-].*") ||

        statement.matches(".*[^)\\d]") || statement.matches(".*[\\d]+[.][\\d]+[.].*")) {

             return null;
        }
        //count if "(" and ")" in the statement are the same amount
        int opening = 0, closing = 0;
        for(int i = 0; i < statement.length(); i++) {

            if(statement.charAt(i) == '(') opening++;
            if(statement.charAt(i) == ')') closing++;
        }
        if(opening != closing) return null;


        // the algorithm which is known as recursive descent parser

        Result result = plusAndMinus(statement);

        if(String.valueOf(result.acc).equals("Infinity")) return null;
        else if(result.acc % 1 == 0) return String.valueOf((int) result.acc);
        else return String.valueOf(round(result.acc));

    }

    private Result plusAndMinus(String s) {

        Result present = mulAndDiv(s);
        double acc = present.acc;

        while (present.rest.length() > 0) {

            if (!(present.rest.charAt(0) == '+' || present.rest.charAt(0) == '-')) break;

            char sign = present.rest.charAt(0);
            String next = present.rest.substring(1);

            present = mulAndDiv(next);
            if (sign == '+') {
                acc += present.acc;
            } else {
                acc -= present.acc;
            }
        }
        return new Result(acc, present.rest);
    }

    private Result mulAndDiv(String s) {

        Result present = parentheses(s);

        double acc = present.acc;
        while (true) {
            if (present.rest.length() == 0) {
                return present;
            }
            char sign = present.rest.charAt(0);
            if ((sign != '*' && sign != '/')) return present;

            String next = present.rest.substring(1);
            Result right = parentheses(next);

            if (sign == '*') {
                acc *= right.acc;
            } else {
                acc /= right.acc;
            }

            present = new Result(acc, right.rest);
        }
    }

    private Result parentheses (String s) {

        char zeroChar = s.charAt(0);
        if (zeroChar == '(') {
            Result r = plusAndMinus(s.substring(1));
            if (!r.rest.isEmpty() && r.rest.charAt(0) == ')') {
                r.rest = r.rest.substring(1);
            }
            return r;
        }
        return getNumber(s);
    }

    private Result getNumber(String s) {

        boolean neg = false;
        // the number can start with minus
        if( s.charAt(0) == '-' ){
            neg = true;
            s = s.substring(1);
        }
        // get the number
        int i = 0;
        while (i < s.length() && (Character.isDigit(s.charAt(i)) || s.charAt(i) == '.')) i++;

        double doubN = Double.parseDouble(s.substring(0, i));
        if(neg) doubN = -doubN;
        String rest = s.substring(i);

        return new Result(doubN, rest);
    }

    private class Result {

        public double acc;     // accumulator
        public String rest;    // the rest of the string we evaluate

        public Result(double d, String s) {
            this.acc = d;
            this.rest = s;
        }
    }

    private static double round(double d) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
