package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder { 

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
		
		if(inputNumbers.contains(null) || inputNumbers.size() < 3) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        int index = 3, rows = 2, colons = 3;
        int addition = 3;

        while (index <= size) {
            if(index < size) {
                rows++;
                colons += 2;
                index += addition;
                addition++;
            }
            else if(index == size) break;
        }

        if(index != size) {
            throw new CannotBuildPyramidException();
        }

        //cheking for the same numbers
        int prev = inputNumbers.get(0);
        for(int i = 1; i < inputNumbers.size(); i++) {
            int current = inputNumbers.get(i);
            if(current == prev) throw new CannotBuildPyramidException();
            prev = current;
        }

        Collections.sort(inputNumbers);
        int[][] array = new int[rows][colons];

        for(int[] subArray : array) {
            for(int x : subArray) {
                x = 0;
            }
        }

        int center = colons/2;
        int indexOfList = 0;

        for(int i = 1; i <= rows; i++) {

           if(i % 2 != 0) { //for odd rows as the first, the third
               ArrayList<Integer> numbersOfElemsInRows = new ArrayList<>();

               numbersOfElemsInRows.add(center);

               for(int s = 1; s <= i/2; s++) {
                   numbersOfElemsInRows.add(center - (2 * s));
                   numbersOfElemsInRows.add(center + (2 * s));
               }
               Collections.sort(numbersOfElemsInRows);

               for(Integer x : numbersOfElemsInRows) {
                   array[i - 1][x] = inputNumbers.get(indexOfList++);
               }
           }
           else { //for even rows

               ArrayList<Integer> numbersOfElemsInRows = new ArrayList<>();

               numbersOfElemsInRows.add(center - 1);
               numbersOfElemsInRows.add(center + 1);

               for(int s = 1; s < i/2; s++) {

                   numbersOfElemsInRows.add(center - 1 - (2 * s));
                   numbersOfElemsInRows.add(center + 1 + (2 * s));
               }
               Collections.sort(numbersOfElemsInRows);

               for(Integer x : numbersOfElemsInRows) {
                   array[i - 1][x] = inputNumbers.get(indexOfList++);
               }
           }
        }
        return array;
    }
}